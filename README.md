# Whatsapp Link Generator

Generates a Whatsapp link that when clicked sends a message to a certain  number

<br>

#### Note:
To run the program, copy-paste `LinkGen.cpp` it into a file named `LinkGen.cpp` on your computer, then type in the CMD:

```C++
g++ -o LinkGen LinkGen.cpp
```
