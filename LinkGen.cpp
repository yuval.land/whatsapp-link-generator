#include <iostream>
#include <string>
#include <cctype>
#include <iomanip>
#include <sstream>

using namespace std;

string url_encode(const string &value) {
    ostringstream escaped;
    escaped.fill('0');
    escaped << hex;

    for (string::const_iterator i = value.begin(), n = value.end(); i != n; ++i) {
        string::value_type c = (*i);

        // Keep alphanumeric and other accepted characters intact
        if (isalnum(c) || c == '-' || c == '_' || c == '.' || c == '~') {
            escaped << c;
            continue;
        }

        // Any other characters are percent-encoded
        escaped << uppercase;
        escaped << '%' << setw(2) << int((unsigned char) c);
        escaped << nouppercase;
    }

    return escaped.str();
}

int main(void)
{
    string phone, message;


    cout << "Created by Yuval Meshorer" << endl << endl;
    cout << "Please enter the phone number: (e.g. 972501235678)\n>>> ";
    cin >> phone;
    getchar();
    cout << "Please enter the message: (e.g. Hello World!)\n>>> ";
    getline(cin, message);

    cout << endl << "https://api.whatsapp.com/send?phone=" << phone << "&text=" << url_encode(message) << endl;

    return 0;
}
